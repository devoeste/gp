package mx.oeste.ml.Dao;

import java.util.List;

public interface Generic {

	public Boolean create(Object persistentObject) throws Exception;
	public Object get(Integer id,@SuppressWarnings("rawtypes") Class c) throws Exception;
	public List<Object> getAll(@SuppressWarnings("rawtypes") Class c) throws Exception;
	public Boolean update(Object persistentObject) throws Exception;
	public Boolean createOrUpdate(Object persistentObject) throws Exception;
	public Boolean delete(Object persistentObject) throws Exception;		
	public List<Object> consulta(String consulta);
	public Boolean insertAll(List<? extends Object> persistentObjects) throws Exception;
	public Boolean updateAll(List<? extends Object>  lista);
	public List<Object> sql(String sql);
	
}
