package mx.oeste.ml.Repository;

import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;

import mx.oeste.ml.Model.AppUser;

public interface AppUserRepository extends SolrCrudRepository<AppUser, String>{

	@Query(value="*:*",filters={"username:?0"})
	public Iterable<AppUser> searchByUsername(String usuario);
}
