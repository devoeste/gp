package mx.oeste.ml.Repository;


import org.springframework.data.solr.repository.SolrCrudRepository;

import mx.oeste.ml.Model.AppMetroCubico2;

public interface AppMetroCubicoRepository2 extends SolrCrudRepository<AppMetroCubico2, String>{

}
