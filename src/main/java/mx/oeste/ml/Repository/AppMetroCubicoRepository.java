package mx.oeste.ml.Repository;



import java.util.List;

import org.springframework.data.geo.Distance;
import org.springframework.data.solr.core.geo.Point;
import org.springframework.data.solr.repository.SolrCrudRepository;

import mx.oeste.ml.Model.AppMetroCubico;

public interface AppMetroCubicoRepository extends SolrCrudRepository<AppMetroCubico, String>{

	public List<AppMetroCubico> findByLocationWithin(Point location, Distance distance);
}
