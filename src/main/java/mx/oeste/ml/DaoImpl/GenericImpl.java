package mx.oeste.ml.DaoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import mx.oeste.ml.Dao.Generic;

@Repository("generic")
public class GenericImpl implements Generic {

	@Autowired
	@Qualifier("sfmejorlugar")
	SessionFactory sf;

	Session session;

	public Boolean create(Object persistentObject) throws Exception {
		session=sf.getCurrentSession();
		session.save(persistentObject);
		return true;
	}

	public Object get(Integer id,@SuppressWarnings("rawtypes") Class c) throws Exception{
		session = sf.getCurrentSession();
		Object value = session.get(c, id);
		if (value == null) {
			return null;
		}
		if (value instanceof HibernateProxy) {
			Hibernate.initialize(value);
			value =((HibernateProxy) value).getHibernateLazyInitializer().getImplementation();
		}
		return value;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getAll(@SuppressWarnings("rawtypes") Class c) throws Exception{
		session = sf.getCurrentSession();
		Criteria crit = session.createCriteria(c);
		return (List<Object>) crit.list();
	}

	public Boolean update(Object persistentObject) throws Exception{
		session = sf.getCurrentSession();
		session.merge(persistentObject);
		return true;
	}

	public Boolean createOrUpdate(Object persistentObject) throws Exception{
		session = sf.getCurrentSession();
		session.saveOrUpdate(persistentObject);
		return true;
	}

	public Boolean delete(Object persistentObject) throws Exception{
		session = sf.getCurrentSession();
		session.delete(persistentObject);
		session.flush();
		return true;
	}

	public Boolean insertAll(List<? extends Object> persistentObjects) throws Exception {
		session = sf.getCurrentSession();
		int i = 0;
		for (Object o : persistentObjects) {
			session.save(o);
			if (i % 100 == 0) {
				session.flush();
				session.clear();
				i = 0;
			}
			i++;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<Object> consulta(String consulta) {
		session=sf.getCurrentSession();
		Query q=session.createQuery(consulta);
		return q.list();
	}


	public Boolean updateAll(List<? extends Object> lista) {
		session = sf.getCurrentSession();
		int i = 0;
		for (Object o : lista) {
			session.update(o);
			if (i % 100 == 0) {
				session.flush();
				session.clear();
				i = 0;
			}
			i++;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> sql(String sql) {
		session=sf.getCurrentSession();
		Query q=session.createSQLQuery(sql);
		return q.list();
	}

}
