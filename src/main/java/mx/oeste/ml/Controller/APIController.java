package mx.oeste.ml.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.geo.Point;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import mx.oeste.ml.Model.AppMetroCubico;
import mx.oeste.ml.Model.AppMetroCubico2;
import mx.oeste.ml.Repository.AppMetroCubicoRepository;
import mx.oeste.ml.Repository.AppMetroCubicoRepository2;
import mx.oeste.ml.Service.CRUDService;
import mx.oeste.ml.Util.AESOeste;

@RestController
@RequestMapping(value = "/rest/api/1.0")
public class APIController {

	@Autowired
	private CRUDService crudService;
	
	@Autowired
	private AppMetroCubicoRepository appMetroCubicoRepository;
	
	@Autowired
	private AppMetroCubicoRepository2 appMetroCubicoRepository2;
	
	@RequestMapping(value="/getInforme", method= RequestMethod.GET , produces = {"application/json"})
	public List<Object> getInforme(@RequestParam("x") Double x,@RequestParam("y") Double y,@RequestParam("m") Integer m){
//		return crudService.sql("SELECT fn_reporte("+x+", "+y+", "+m+");");
		return crudService.sql("SELECT fn_demo_grupo_presidente("+x+", "+y+", "+m+");");
		
	}
	@RequestMapping(value="/getCoordinate", method= RequestMethod.GET , produces = {"application/json"})
	public List<Object> getCoordinate(@RequestParam("x") Integer x,@RequestParam("y") Integer y){
		return crudService.sql("SELECT fn_coordenadas("+x+", "+y+");");		
	}
	@RequestMapping(value="/getRoute", method= RequestMethod.GET , produces = {"application/json"})
	public List<Object> getRoute(@RequestParam("p") Integer p,@RequestParam("x") Double x ,@RequestParam("y") Double y){
		return crudService.sql("SELECT fn_dgp_ruta("+p+", "+x+", "+y+");");		
	}
	
	@RequestMapping(value="/getGeoJson", method=RequestMethod.GET , produces= {"application/json"})
	public List<Object> getGeoJson()
	{
		return crudService.sql("SELECT public.fn_compra_venta_color();");
	}
	@RequestMapping(value="/xl", method= RequestMethod.POST)
	public String encoderO(@RequestBody String s){
		AESOeste es = new AESOeste();
		return es.EncrytAES(s);
	}
	
	@RequestMapping(value="/procesar", method= RequestMethod.POST)
	public Iterable<AppMetroCubico2> procesar(){
		appMetroCubicoRepository.deleteAll();
		Iterable<AppMetroCubico2> w = appMetroCubicoRepository2.findAll();
		for(AppMetroCubico2 ww :w ){
			AppMetroCubico q= new AppMetroCubico();
			q.setLiga(ww.getLiga());
			q.setTitulo(ww.getTitulo());
			q.setDireccion(ww.getDireccion());
			q.setPrecioEnDolares(ww.getPrecioEnDolares());
			q.setInmueble(ww.getInmueble());
			q.setImagenes(ww.getImagenes());
			q.setOperacion(ww.getOperacion());
			q.setNiveles(ww.getNiveles());
			q.setLuminosidad(ww.getLuminosidad());
			q.setCaracteristicasAdiscionales(ww.getCaracteristicasAdiscionales());
			if(ww.getLatLon()!=null)
				if(!ww.getLatLon().isEmpty()){
					String[] www = ww.getLatLon().get(0).split(",");
					Point pw = new Point(new Double(www[0]),new Double(www[1]));
					q.setLocation(pw);
				}
			q.setArea(ww.getArea());
			q.setBanos(ww.getBanos());
			q.setCuotaMantenimiento(ww.getCuotaMantenimiento());
			q.setElevador(ww.getElevador());
			q.setHorarioContacto(ww.getHorarioContacto());
			q.setPrecio(ww.getPrecio());
			q.setRef(ww.getRef());
			q.setSuperficieConstruida(ww.getSuperficieConstruida());
			q.setSuperficieTotal(ww.getSuperficieTotal());
			appMetroCubicoRepository.save(q);
		}
		return w;
	}
}
