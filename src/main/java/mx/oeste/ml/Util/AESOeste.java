package mx.oeste.ml.Util;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;


public class AESOeste {

	private String hash="ass4RWx5h0a4H38o";
	private Key clave;
	private Cipher c;

	public AESOeste(){
		this.clave = new SecretKeySpec(hash.getBytes(), "AES");
	}
	
	public String EncrytAES(String Password){
		try {
			c = Cipher.getInstance("AES/ECB/PKCS5Padding");
			c.init(Cipher.ENCRYPT_MODE, this.clave);
			byte[] encValor = c.doFinal(Password.getBytes());
			return new Base64().encodeToString(encValor);
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public String AES(String Hash){
		try {
			c = Cipher.getInstance("AES/ECB/PKCS5Padding");
			c.init(Cipher.DECRYPT_MODE, this.clave);
			@SuppressWarnings("static-access")
			byte[] valorDecodificado = (new Base64()).decodeBase64(Hash);
			byte[] decValor = c.doFinal(valorDecodificado);
			return new String(decValor);
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
