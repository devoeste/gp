package mx.oeste.ml.Model;

import java.util.List;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;




@SolrDocument(solrCoreName = "metrosCubicos")
public class AppMetroCubico2 {

	@Id
	@Field
	private String id;
	
	@Field
	private String liga;
	
	@Field
	private String titulo;
	
	@Field 
	private String direccion;
	
	@Field 
	private Boolean precioEnDolares;
	
	@Field 
	private String inmueble;
	
	@Field 
	private List<String> imagenes;
	
	@Field 
	private String operacion;
	
	@Field 
	private Integer niveles;
	
	@Field 
	private String luminosidad;
	
	@Field 
	private List<String> caracteristicasAdiscionales;

	@Field 
	private List<String> latLon;
	
	@Field 
	private Double area;
	
	@Field 
	private Long banos;
	
	@Field 
	private Long cuotaMantenimiento;
	
	@Field 
	private Boolean elevador;

	@Field 
	private String horarioContacto;
	
	@Field 
	private Double precio;
	
	@Field 
	private Long ref;
	
	@Field 
	private Double superficieConstruida;
	
	@Field 
	private Double superficieTotal;
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLiga() {
		return liga;
	}

	public void setLiga(String liga) {
		this.liga = liga;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public Boolean getPrecioEnDolares() {
		return precioEnDolares;
	}

	public void setPrecioEnDolares(Boolean precioEnDolares) {
		this.precioEnDolares = precioEnDolares;
	}

	public String getInmueble() {
		return inmueble;
	}

	public void setInmueble(String inmueble) {
		this.inmueble = inmueble;
	}

	public List<String> getImagenes() {
		return imagenes;
	}

	public void setImagenes(List<String> imagenes) {
		this.imagenes = imagenes;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public Integer getNiveles() {
		return niveles;
	}

	public void setNiveles(Integer niveles) {
		this.niveles = niveles;
	}

	public String getLuminosidad() {
		return luminosidad;
	}

	public void setLuminosidad(String luminosidad) {
		this.luminosidad = luminosidad;
	}

	public List<String> getCaracteristicasAdiscionales() {
		return caracteristicasAdiscionales;
	}

	public void setCaracteristicasAdiscionales(List<String> caracteristicasAdiscionales) {
		this.caracteristicasAdiscionales = caracteristicasAdiscionales;
	}

	public List<String> getLatLon() {
		return latLon;
	}

	public void setLatLon(List<String> latLon) {
		this.latLon = latLon;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public Long getBanos() {
		return banos;
	}

	public void setBanos(Long banos) {
		this.banos = banos;
	}

	public Long getCuotaMantenimiento() {
		return cuotaMantenimiento;
	}

	public void setCuotaMantenimiento(Long cuotaMantenimiento) {
		this.cuotaMantenimiento = cuotaMantenimiento;
	}

	public Boolean getElevador() {
		return elevador;
	}

	public void setElevador(Boolean elevador) {
		this.elevador = elevador;
	}

	public String getHorarioContacto() {
		return horarioContacto;
	}

	public void setHorarioContacto(String horarioContacto) {
		this.horarioContacto = horarioContacto;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Long getRef() {
		return ref;
	}

	public void setRef(Long ref) {
		this.ref = ref;
	}

	public Double getSuperficieConstruida() {
		return superficieConstruida;
	}

	public void setSuperficieConstruida(Double superficieConstruida) {
		this.superficieConstruida = superficieConstruida;
	}

	public Double getSuperficieTotal() {
		return superficieTotal;
	}

	public void setSuperficieTotal(Double superficieTotal) {
		this.superficieTotal = superficieTotal;
	}


	
}

	