package mx.oeste.ml.ServiceImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import mx.oeste.ml.Model.AppUser;
import mx.oeste.ml.Repository.AppUserRepository;
import mx.oeste.ml.Util.AESOeste;


@Service("CustomUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService{
	
	//private static final Logger logger= LoggerFactory.

	@Autowired
	AppUserRepository appUserRepository;
	
	public List<String> getRoles(long role){
		List<String> roles = new ArrayList<String>();
		roles.add("xD");
		return roles;
	}
	
	public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles){
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for(String role : roles){
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}
	
	public Collection <? extends GrantedAuthority> getAuthorities(long role){
		List <GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
		return authList;
	}

	public UserDetails loadUserByUsername(String arg0) {
		AppUser pecUsuario = null;
		Iterable<AppUser> users = null;
		AESOeste es = new AESOeste();
		try {
			users = appUserRepository.searchByUsername(arg0);
			if(users.iterator().hasNext())
				pecUsuario=users.iterator().next();
			else{
				throw new Exception("Sin datos");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new User( pecUsuario.getUsername(), 
				es.AES(pecUsuario.getPassword()), 
				true, 
				true, 
				true, 
				true, 
				getAuthorities(new Long("1"))
		);	
	}

	
}
