package mx.oeste.ml.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.oeste.ml.Model.AppUser;
import mx.oeste.ml.Repository.AppUserRepository;
import mx.oeste.ml.Service.AppUserService;

@Service("appUserService")
public class AppUserServiceImpl implements AppUserService {

	@Autowired
	private AppUserRepository appUserRepository;
	
	public Iterable<AppUser> getAllMC() {
		return appUserRepository.findAll();
	}

}
