package mx.oeste.ml.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.oeste.ml.Dao.Generic;
import mx.oeste.ml.Service.CRUDService;

@Service("Crud")
@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
public class CRUDServiceImpl implements CRUDService {

	@Autowired
	@Qualifier("generic")
	Generic dao;

	public Boolean create(Object persistentObject)  {
		try {
			return dao.create(persistentObject);
		} catch (Exception e) {
			return false;
		}

	}

	public Object get(Integer id,@SuppressWarnings("rawtypes") Class c) {
		try {
			return dao.get(id, c);
		} catch (Exception e) {
			return false;
		}

	}

	public List<Object> getAll(@SuppressWarnings("rawtypes") Class c) {
		try {
			return dao.getAll(c);
		} catch (Exception e) {
			return null;
		}

	}

	public Boolean update(Object persistentObject) {
		try {
			return dao.update(persistentObject);
		} catch (Exception e) {
			return false;
		}

	}

	public Boolean createOrUpdate(Object persistentObject) {
		try {
			return dao.createOrUpdate(persistentObject);
		} catch (Exception e) {
			return false;
		}

	}

	public Boolean delete(Object persistentObject) {
		try {
			return dao.delete(persistentObject);
		} catch (Exception e) {
			return false;
		}

	}

	public Boolean insertAll(List<? extends Object> persistentObjects) {
		try {
			dao.insertAll(persistentObjects);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public List<Object> consulta(String consulta) {
		try {
			return dao.consulta(consulta);
		} catch (Exception e) {
			return null;
		}
	}


	public Boolean updateAll(List<? extends Object>  lista) {
		try {
			dao.insertAll(lista);
			return true;
		} catch (Exception e) {
			return false;
		}

	}
	
	public List<Object> sql(String sql) {
		try {
			return dao.sql(sql);
		} catch (Exception e) {
			return null;
		}
	}
}
