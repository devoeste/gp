package mx.oeste.ml.Service;

import java.util.List;

public interface CRUDService {
	public Boolean create(Object persistentObject);
	public Object get(Integer id,@SuppressWarnings("rawtypes") Class c);
	public List<Object> getAll(@SuppressWarnings("rawtypes") Class c);
	public Boolean update(Object persistentObject);
	public Boolean createOrUpdate(Object persistentObject);
	public Boolean delete(Object persistentObject);
	public Boolean insertAll(List<? extends Object> persistentObjects);
	public List<Object> consulta(String consulta);
	public Boolean updateAll(List<? extends Object>  lista);
	public List<Object> sql(String sql);
}
