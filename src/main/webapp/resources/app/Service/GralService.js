angular.module("appMl.GralService").factory("GralService", ['$http', '$q', function($http, $q){
	
	return {
			dataSpacial: function(x,y,m){
				return $http.post('/ml/rest/api/1.0/getInforme/', {'x':x,'y':y,'m':m});
			}
	    
	};

}]);
