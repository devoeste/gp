angular.module("appLogin.loginCtrl").controller("loginCtrl", function($scope) {
	
	$scope.init = function(){
		$scope.alerta = "";
		$scope.muestra=false;
		var url= window.location.href;
		if(url.indexOf('error=403')!= -1){
			$scope.muestra=true;
			$scope.alerta = "Credenciales erroneas";
		}
	}

});