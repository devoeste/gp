google.charts.load('current', {'packages':['corechart']});
$(function() {
	 $.fn.onEnter = function(func) {
	        this.bind('keypress', function(e) {
	            if (e.keyCode == 13) func.apply(this, [e]);    
	        });               
	        return this; 
	     };
	
	  var latSolr = 0;
	  var lngSolr = 0;
	  var startSolr = 0;
	  
	  var totalM2v=0;
	  var totalM2r=0;
	  
	  var precioM2v=0;
	  var precioM2r=0;
	  
	  var dlls=20;
	  
	  var totalLigas=0;
	  var totalLigasR=0;

	  var $handle;

	  $('input[type="range"]').rangeslider({
	      polyfill: false,
	      onInit: function() {
	    	  $('#mts').val(this.value);
	      }
	    })
	    .on('input', function() {
	    	$('#mts').val(this.value);

	    });
	    
  $("a.tablinks").click(function(evt){
    var contInfo= ($(this).attr('data'));
    var contInd= ($(this).attr('data-index'));
//    contInfo.setVisibility(true)
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(contInfo).style.display = "block";
    var lay=document.getElementById(contInfo);
    evt.currentTarget.className += " active";
    
    		 $.each(map.getLayers().getArray(), function(key,value) {
			 try{
				 var features = source.getFeatures();
				 if (features != null && features.length > 0){
					 for (x in features){
						 source.removeFeature(features[x]);
						 }
				 }

				if (value!=undefined) {
					if(value.getZIndex()>1 && value.getZIndex()==contInd){
						value.setVisible(true);
					}
				    if(value.getZIndex()>1 && value.getZIndex()!=contInd){
						value.setVisible(false);						
					}
			 }
			 } catch(e){	
				 console.log(e)
			 }
		 });
   if(arrayids.length!=0){
	   for(ind=0;ind<arrayids.length;ind++){
		   document.getElementById(arrayids[ind]).style.background = '#000000';
		   $('#'+arrayids[ind]+'icon').empty();
		   }	   
	   arrayids = arrayids.splice(0,0);
	   arrayimg = arrayimg.splice(0,0);
   } 	    
    
  });

	  
	  var image = new ol.style.Circle({
	        radius: 5,
	        fill: null,
	        stroke: new ol.style.Stroke({color: 'red', width: 1})
	      });

	      var styles = {
	        'Point': new ol.style.Style({
	          image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
	        	    anchor: [0.5, 46],
	        	    anchorXUnits: 'fraction',
	        	    anchorYUnits: 'pixels',
	        	    opacity: 0.75,
	        	    src: 'resources/img/pin0.png'
	        	  }))
	        }),
	        
	        'Circle': new ol.style.Style({
	          stroke: new ol.style.Stroke({
	            color: '#F7BE81',
	            width: 2
	          }),
	          fill: new ol.style.Fill({
	            color: 'rgba(245,200,152,0.2)'
	          })
	        }),
	        
	        'LineString': new ol.style.Style({
	            stroke: new ol.style.Stroke({
	              color: 'green',
	              width: 1
	            })
	          }),
	      };

	      var styles1 = {
	    		  'Point': new ol.style.Style({
	    	    image: new ol.style.Circle({

	    	      fill: new ol.style.Fill({
	    	        color: '#445BA6'
	    	      }),
	    	      stroke: new ol.style.Stroke({
	    	        color: '#364985',
	    	        width: 1
	    	      }),
	    	      radius: 5
	    	    })
	    	  })
	      };

	      var styles2 = {
	    		  'Point': new ol.style.Style({
	  	    	    image: new ol.style.Circle({

	  	    	      fill: new ol.style.Fill({
	  	    	        color: '#5FA644'
	  	    	      }),
	  	    	      stroke: new ol.style.Stroke({
	  	    	        color: '#4C8436',
	  	    	        width: 1
	  	    	      }),
	  	    	      radius: 5
	  	    	    })
	  	    	  })
	      };

	      var styleFunction = function(feature) {
	        return styles[feature.getGeometry().getType()];
	      };

	      var styleFunction1 = function(feature) {
		        return styles1[feature.getGeometry().getType()];
		      };

		      var styleFunction2 = function(feature) {
			        return styles2[feature.getGeometry().getType()];
			      };

	
	      var geojsonObject = {
	    	        'type': 'FeatureCollection',
	    	        'crs': {
	    	          'type': 'name',
	    	          'properties': {
	    	            'name': 'EPSG:4326'
	    	          }
	    	        },
	    	        'features': []
	    	      };
	 
	      var geojsonObject2 = {
	    	        'type': 'FeatureCollection',
	    	        'crs': {
	    	          'type': 'name',
	    	          'properties': {
	    	            'name': 'EPSG:4326'
	    	          }
	    	        },
	    	        'features': []
	    	      };
	 
	 var vectorSource = new ol.source.Vector({
	        features: (new ol.format.GeoJSON()).readFeatures(geojsonObject)
	      });

	 var vectorSource2 = new ol.source.Vector({
	        features: (new ol.format.GeoJSON()).readFeatures(geojsonObject2)
	      });
	 
	 var vectorSource3 = new ol.source.Vector({
	        features: (new ol.format.GeoJSON()).readFeatures(geojsonObject2)
	      });
	 
	      
//------------------
	 var source = new ol.source.Vector({
        wrapX: false
      });
      var vector = new ol.layer.Vector({
        source: source
      });
      var arraystatus = new Array();	    
      function removeFeature(ide){
		var features = source.getFeatures();
			 if (features != null && features.length > 0){
				 for (x in features){
					 var features = source.getFeatures();
					 var fea = features[x];
					 var idfea = fea.get('id');
					 if(ide==idfea){
						 source.removeFeature(features[x]);
						 break;
					 }
			}
		}
		
		$.each(map.getLayers().getArray(), function(key,value) {
			 try{
				if (value!=undefined && value.get('id')!=undefined) {			
					if(ide==value.get('id')){
//						console.log("value "+value.getZIndex());
						map.removeLayer(value);
					}
			 }
			 } catch(e){	
				 console.log(e)
			 }
		 });
      }
      function addFeature(cx, cy, ide, rutas, ind) {
        var x = cx;
        var y = cy;

        var geom = new ol.geom.Point([x, y]);
        var feature = new ol.Feature({
            id: ide,
            geometry:geom
            });
        source.addFeature(feature);
        var style = new ol.style.Style({
	          image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
	        	    anchor: [0.5, 46],
	        	    anchorXUnits: 'fraction',
	        	    anchorYUnits: 'pixels',
	        	    opacity: 0.75,
	        	    src: 'resources/img/point'+ind+'.svg'
	        	  }))
        });
        arraystatus[ind]=true
        feature.setStyle(style);

        var rut = new ol.layer.Tile({
        	id:ide,
            type: 'Overlays ',
    	    visible: true,  
    		zIndex:10,
          source: new ol.source.TileWMS({
				 url: 'http://158.69.229.35/geoserver/Oeste/wms',
					serverType:'geoserver',
					params:{
						LAYERS: 'Oeste:layer_ml',
			            opacity: 0.2,
			            TRANSPARENT: true,
			            STYLES:'Viales',
			            VIEWPARAMS:'tipo:5;list:'+rutas
					}
				})
        })
        map.addLayer(rut);
      }

  
      //---------------
      
      
	 var view = new ol.View({
       center: [-99.18655,19.43798],
       zoom: 12,
       minZoom:6,
       maxZoom: 19,
       projection: 'EPSG:4326'
     });

	 var styleCache = {};
	 
	 mover = function(a,b){ 
		 do{
		 $.each(map.getLayers().getArray(), function(key,value) {
			 try{
				 var features = source.getFeatures();
				 if (features != null && features.length > 0){
					 for (x in features){
						 source.removeFeature(features[x]);
						 }
				 }

				map.removeLayer(vector)
				if (value!=undefined) {
					if(value.getZIndex()>1){
//						console.log("value "+value.getZIndex());
						map.removeLayer(value);
					}
			 }
			 } catch(e){	
				 console.log(e)
			 }
		 });}while(map.getLayers().getArray().length>2)
		 
		 console.log("  "+a+"  "+b+"  "+parseInt($('#mts').val()));
		 $.getJSON( "/gp/rest/api/1.0/getInforme/", { 'x': a, 'y': b ,'m': parseInt($('#mts').val())}, function( response ) {
				var gral =  JSON.parse(response[0]);
				map.getView().setCenter([a,b]);	
//				vectorSource.addFeature(new ol.Feature(new ol.geom.LineString([[gral.bbox[0].xmax,gral.bbox[0].ymax],[gral.bbox[0].xmin,gral.bbox[0].ymin]])));
				var polygon = /** @type {ol.geom.LineString} */ (new ol.Feature(new ol.geom.LineString([[gral.bbox[0].xmax,gral.bbox[0].ymax],[gral.bbox[0].xmin,gral.bbox[0].ymin]])).getGeometry());
		        var size = /** @type {ol.Size} */ (map.getSize());
		        view.fit(polygon, size, {padding: [10, 10, 10, 10], constrainResolution: false});
		        
		        				//table typology
		    	var tabletypology=[["", "zona", { role: "style" } ]];

		    	var typology=gral.data_tipology;
		    	var divto='dataTo';
		    	var infotypology=[];
		    	var auxrow
		    	$.each(typology, function(key,value) {
		    		var arrayint=[];
		    		arrayint.push("");
		    		arrayint.push(value.n_tipologias);
		    		arrayint.push('stroke-color: #000077; stroke-width: .5; stroke-opacity: 0.8; fill-color:'+ value.ci_color+'; fill-opacity: 0.8;');
//		    		arrayint.push('stroke-color:'+ value.ci_color+'; fill-color:'+ value.ci_color+';');
//		    		if(key==(typology.length-1) || key==0){
//		    			if(key==0)
//		    				arrayint.push(value.ci_total+''+value.nombre_tip);
//		    			if((key==(typology.length-1))&&(key!=00))
//		    				arrayint.push(value.ci_total+''+value.nombre_tip);
//		    		}
//		    		else
		    			arrayint.push(value.n_tipologias+' zonas donde reciden '+value.nombre_tip);
		    		infotypology.push(arrayint);
	    	});
	    			        google.charts.setOnLoadCallback(drawChart(infotypology, divto));



		        var topology="";
		    	$.each(gral.id_typology, function(key,value) {
		    		topology+=value+"\\,";
		    	});
		    	topology= topology.substring(0,topology.length-2);
		    	var t = new ol.layer.Tile({
			    		title: 'Tipologias',
		                type: 'Overlays	',
		        	    visible: false,  
			    		zIndex:10,
		              source: new ol.source.TileWMS({
							 url: 'http://158.69.229.35/geoserver/Oeste/wms',
								serverType:'geoserver',
								params:{
									LAYERS: 'Oeste:layer_ml',
						            opacity: 0.2,
						            TRANSPARENT: true,
						            STYLES:'ml',
						            VIEWPARAMS:'tipo:4;list:'+topology
								}
							})
		            })

		    	var resultado="";
		    	$('#dataMunicipios').empty();
		    	$.each(gral.id_delegacion, function(key,value) {
		    		resultado+=value.ci_capa_geom_id+"\\,";
		    		$('#dataMunicipios').append("<div class=\"getinf\"><p ALIGN=right style=\"color: #2e3c43;white-space: " +
				    		 "nowrap;overflow: hidden; text-overflow: ellipsis;font-size: 26px;font-weight: 400;" +
				    		 " line-height: 28px;\">"+value.nombre+"</p></div>");
		    	});
		    	resultado= resultado.substring(0,resultado.length-2);
		    	var s = new ol.layer.Tile({
			    		title: 'Municipios',
		                type: 'Overlays	',
		        	    visible: false,  
			    		zIndex:2,
		              source: new ol.source.TileWMS({
							 url: 'http://158.69.229.35/geoserver/Oeste/wms',
								serverType:'geoserver',
								params:{
									LAYERS: 'Oeste:layer_ml',
						            opacity: 0.2,
						            TRANSPARENT: true,
						            STYLES:'ml',
						            VIEWPARAMS:'tipo:0;list:'+resultado
								}
							})
		            })
	    		var hoteles="";
		    	$('#dataHoteles').empty();
		    	$.each(gral.solo_72, function(key,value) {
			    		hoteles+=value.ci_capa_geom_id+"\\,";
			    		$('#dataHoteles').append("<div id=\""+value.ci_capa_geom_id+"\" class=\"getinf\" style=\"background:#ffffff !important\"><div id=\""+value.ci_capa_geom_id+"icon\" class=\"getinficon\" ></div><p ALIGN=right>"+value.nom_estab+"<br><p ALIGN=right style=\"color: #2e3c43;white-space: " +
			    		 "nowrap;overflow: hidden; text-overflow: ellipsis;font-size: 26px;font-weight: 400;" +
			    		 " line-height: 24px;\">"+value.distance+"m"+"</p></p></div>");
		    	});

	    		hoteles= hoteles.substring(0,hoteles.length-(2));
	    		
	    		
		    	var g = new ol.layer.Tile({
		    		title: 'Hoteles',
	                type: 'Overlays ',
	        	    visible: false,  
		    		zIndex:8,
	              source: new ol.source.TileWMS({
						 url: 'http://158.69.229.35/geoserver/Oeste/wms',
							serverType:'geoserver',
							params:{
								LAYERS: 'Oeste:layer_ml',
					            opacity: 0.2,
					            TRANSPARENT: true,
					            STYLES:'mlhoteles',
					            VIEWPARAMS:'tipo:6;list:'+hoteles
							}
						})
	            })
		    	var generador_demanda="";
		    	$('#dataGen').empty();
		    	$.each(gral.sin_72, function(key,value) {
			    		generador_demanda+=value.ci_capa_geom_id+"\\,";
			    		$('#dataGen').append("<div id=\""+value.ci_capa_geom_id+"\" class=\"getinf\" style=\"background:#ffffff !important\"><div id=\""+value.ci_capa_geom_id+"icon\" class=\"getinficon\" ></div><p ALIGN=right>"+value.nom_estab+"<br><p ALIGN=right style=\"color: #2e3c43;white-space: " +
			    		 "nowrap;overflow: hidden; text-overflow: ellipsis;font-size: 26px;font-weight: 400;" +
					    		 " line-height: 24px;\">"+value.distance+"m"+"</p></p></div>");
		    	});

	    		generador_demanda= generador_demanda.substring(0,generador_demanda.length-(2));

		    	var gd = new ol.layer.Tile({
		    		title: 'GeneradoresDemanda',
	                type: 'Overlays ',
	        	    visible: false,  
		    		zIndex:9,
	              source: new ol.source.TileWMS({
						 url: 'http://158.69.229.35/geoserver/Oeste/wms',
							serverType:'geoserver',
							params:{
								LAYERS: 'Oeste:layer_ml',
					            opacity: 0.2,
					            TRANSPARENT: true,
					            STYLES:'mlgeneradores',
					            VIEWPARAMS:'tipo:6;list:'+generador_demanda
							}
						})
	            })
		    	var carreteras="";
		    	$('#dataCa').empty();
		    	$.each(gral.capa_9_carretera, function(key,value) {
			    		carreteras+=value.ci_capa_geom_id+"\\,";
			    		$('#dataCa').append("<div class=\"getinf\"><p ALIGN=right>"+value.nombre+"<br><p ALIGN=right style=\"color: #2e3c43;white-space: " +
					    		 "nowrap;overflow: hidden; text-overflow: ellipsis;font-size: 26px;font-weight: 400;" +
					    		 " line-height: 24px;\">"+value.distance+"m"+"</p></p></div>");
		    	});

	    		carreteras= carreteras.substring(0,carreteras.length-(2));
	    		

		    	var c = new ol.layer.Tile({
		    		title: 'Carreteras',
	                type: 'Overlays ',
	        	    visible: false,  
		    		zIndex:5,
	              source: new ol.source.TileWMS({
						 url: 'http://158.69.229.35/geoserver/Oeste/wms',
							serverType:'geoserver',
							params:{
								LAYERS: 'Oeste:layer_ml',
					            opacity: 0.2,
					            TRANSPARENT: true,
					            bgcolor: '#ff7800',
					            STYLES:'mlcarretera',
					            VIEWPARAMS:'tipo:9;list:'+carreteras
							}
						})
	            })
		    	var vias="";
		    	$('#dataFe').empty();
		    	$.each(gral.capa_9_via_ferrea, function(key,value) {
			    		vias=value.ci_capa_geom_id+"\\,";
			    		$('#dataFe').append("<div class=\"getinf\"><p ALIGN=right><p ALIGN=right style=\"color: #2e3c43;white-space: " +
					    		 "nowrap;overflow: hidden; text-overflow: ellipsis;font-size: 26px;font-weight: 400;" +
					    		 " line-height: 24px;\">"+value.distance+"m"+"</p></p></div>");
		    	});

	    		vias= vias.substring(0,vias.length-(2));

		    	var v = new ol.layer.Tile({
		    		title: 'RedFerrea',
	                type: 'Overlays ',
	        	    visible: false,  
		    		zIndex:6,
	              source: new ol.source.TileWMS({
						 url: 'http://158.69.229.35/geoserver/Oeste/wms',
							serverType:'geoserver',
							params:{
								LAYERS: 'Oeste:layer_ml',
					            opacity: 0.2,
					            TRANSPARENT: true,
					            STYLES:'mlvias',
					            VIEWPARAMS:'tipo:9;list:'+vias
							}
						})
	            })
		    	$('#dataAe').empty();
		    	var aeropuerto="";
		    	$.each(gral.capa_9_aeropuerto, function(key,value) {
			    		aeropuerto=value.ci_capa_geom_id+"\\,";
			    		$('#dataAe').append("<div class=\"getinf\"><p ALIGN=right>Aeropuerto Internacional Benito Juárez<br><p ALIGN=right style=\"color: #2e3c43;white-space: " +
					    		 "nowrap;overflow: hidden; text-overflow: ellipsis;font-size: 26px;font-weight: 400;" +
					    		 " line-height: 24px;\">"+value.distance+"m"+"</p></p></div>");
		    	});

	    		aeropuerto= aeropuerto.substring(0,aeropuerto.length-(2));

		    	var a = new ol.layer.Tile({
		    		title: 'Aeropuertos',
	                type: 'Overlays ',
	        	    visible: false,  
		    		zIndex:7,
	              source: new ol.source.TileWMS({
						 url: 'http://158.69.229.35/geoserver/Oeste/wms',
							serverType:'geoserver',
							params:{
								LAYERS: 'Oeste:layer_ml',
					            opacity: 0.2,
					            TRANSPARENT: true,
					            STYLES:'mlaeropuerto',
					            VIEWPARAMS:'tipo:9;list:'+aeropuerto
							}
						})
	            })

				//table renta
		    	var tablerenta=[["", "zona", { role: "style" } ]];

		    	var renta=gral.renta_total;
		    	renta.sort(function (a, b) {
		    		  return a.cn_r_inferior - b.cn_r_inferior;
		    		});
		    	var div='dataRenta';
		    	var inforenta=[];
		    	var auxrow
		    	$.each(renta, function(key,value) {
		    		var arrayint=[];
		    		arrayint.push("");
		    		arrayint.push(value.ci_total);
		    		arrayint.push('stroke-color: #007700; stroke-width: .5; stroke-opacity: 0.8; fill-color:'+ value.ct_color+'; fill-opacity: 0.8;');
//		    		arrayint.push('stroke-color:'+ value.ct_color+'; fill-color:'+ value.ct_color+';');
		    		if(key==(renta.length-1) || key==0){
		    			if(key==0)
		    				arrayint.push(value.ci_total+' zonas a menos de $'+value.cn_r_superior+' por metro cuadrado');
		    			if((key==(renta.length-1))&&(key!=00))
		    				arrayint.push(value.ci_total+' zonas a mas de $'+value.cn_r_inferior+' por metro cuadrado');
		    		}
		    		else
		    			arrayint.push(value.ci_total+' zonas desde $'+value.cn_r_inferior+' hasta $'+value.cn_r_superior+' por metro cuadrado');
		    		inforenta.push(arrayint);
	    	});
		        
		    	var crenta="";
		        google.charts.setOnLoadCallback(drawChart(inforenta, div));
		    	$.each(gral.ageb_color_renta, function(key,value) {
		    		crenta+=value+"\\,";
		    	});
	    		crenta= crenta.substring(0,crenta.length-(2));
		    	var cr = new ol.layer.Tile({
		    		title: 'PrecioRenta',
	                type: 'Overlays ',
	        	    visible: false,  
		    		zIndex:3,
	              source: new ol.source.TileWMS({
						 url: 'http://158.69.229.35/geoserver/Oeste/wms',
							serverType:'geoserver',
							params:{
								LAYERS: 'Oeste:vw_renta',
					            opacity: 0.2,
					            TRANSPARENT: true,
					            STYLES:'ml_agebs',
					            VIEWPARAMS:'code:'+crenta
							}
						})
	            })
		    	
		    	
		    	
				//table venta
		    	var tableventa=[["", "zona", { role: "style" } ]];

		    	var venta=gral.venta_total;
		    	venta.sort(function (a, b) {
		    		  return a.cn_r_inferior - b.cn_r_inferior;
		    		});
		    	var div='dataVenta';
		    	var infoventa=[];
		    	var auxrow
		    	$.each(venta, function(key,value) {
		    		var arrayint=[];
		    		arrayint.push("");
		    		arrayint.push(value.ci_total);
		    		arrayint.push('stroke-color: #000077; stroke-width: .5; stroke-opacity: 0.8; fill-color:'+ value.ct_color+'; fill-opacity: 0.8;');
//		    		arrayint.push('stroke-color:'+ value.ct_color+'; fill-color:'+ value.ct_color+';');
		    		if(key==(venta.length-1) || key==0){
		    			if(key==0)
		    				arrayint.push(value.ci_total+' zonas a mas de $'+value.cn_r_inferior+' por metro cuadrado');
		    			if((key==(venta.length-1))&&(key!=00))
		    				arrayint.push(value.ci_total+' zonas a menos de $'+value.cn_r_superior+' por metro cuadrado');
		    		}
		    		else
		    			arrayint.push(value.ci_total+' zonas desde $'+value.cn_r_inferior+' hasta $'+value.cn_r_superior+' por metro cuadrado');
		    		infoventa.push(arrayint);
	    	});
		        
		    	var cventa="";
		        google.charts.setOnLoadCallback(drawChart(infoventa, div));
		    	$.each(gral.ageb_color_venta, function(key,value) {
		    		cventa+=value+"\\,";
		    	});
	    		cventa= cventa.substring(0,cventa.length-(2));
		    	var cv = new ol.layer.Tile({
		    		title: 'PrecioVenta',
	                type: 'Overlays ',
	        	    visible: false,  
		    		zIndex:4,
	              source: new ol.source.TileWMS({
						 url: 'http://158.69.229.35/geoserver/Oeste/wms',
							serverType:'geoserver',
							params:{
								LAYERS: 'Oeste:vw_venta',
					            opacity: 0.2,
					            TRANSPARENT: true,
					            STYLES:'ml_agebs',
					            VIEWPARAMS:'code:'+cventa
							}
						})
	            })
		    			    			    				
				map.addLayer(s); //delegaciones 2
		    	map.addLayer(cv); //casas venta 3
		    	map.addLayer(cr); //casas renta 4
		    	map.addLayer(c); //carreteras 5
		    	map.addLayer(v); //viasferreas 6
		    	map.addLayer(a); //aeropuertos 7
		    	map.addLayer(g); //hoteles 8
		    	map.addLayer(gd); //generadores de demanda 9
		    	map.addLayer(t); //tipologias 10
		    	$('.gridc').show();
				map.addLayer(vector);
						
			});
		 
		 vectorSource.clear();
		 vectorSource.addFeature(new ol.Feature(new ol.geom.Circle([a,b], parseInt($('#mts').val())/100000)));
		 vectorSource.addFeature(new ol.Feature(new ol.geom.Point([a,b])));
		 latSolr = b;
		 lngSolr = a;
//		 solr();
	 }
	 
	 //capas en el mapa
	 var map = new ol.Map({
	        layers: [
	        	new ol.layer.Tile({
	        		title: 'BingMaps',
	                type: /*'base'*/'Overlays',
	        	    visible: true,
	        	    source: new ol.source.BingMaps({
	        	      key: 'Ag5Sl3HMkgRbxoPJ-wZaCCHZFpQj-uOeIUeQgBQvuqnHx7nT1APozOhqCD6OYT00',
	        	      imagerySet: 	'Road',
	        	    }),
	        		zIndex: 0
	        	}),
	        	new ol.layer.Vector({
	        		title: 'Controles',
	                type: 'Overlays',
	        		visible: true,
	  	        source: vectorSource,
	  	        style: styleFunction,
	  	        zIndex: 1
	  	      })
	        ],
	        target: 'map',
	        interactions : ol.interaction.defaults({doubleClickZoom :false}),
	        controls: ol.control.defaults({
	          attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
	            collapsible: false
	          })
	        }),
	        view: view
	    });
	    
	    searchServiceCallback = function(data){
	    	$.each(data.resourceSets, function( key,value) {
	    		$.each(value.resources, function( key2,value2) {
	                mover(value2.point.coordinates[1],value2.point.coordinates[0]);
	            });
	        });
	    }
	    
	    
	    callback = function(data){
	    	if(startSolr==0){
	    		vectorSource2.clear();
	    		vectorSource3.clear();
	    	}
	    	 startSolr+=100;
	    	 $.each( data.response.docs, function( key, value ) {
	    		 var res = value.location[0].split(",");
	    		 if(value.operacion == 'Venta'){
	    			 vectorSource2.addFeature(new ol.Feature(new ol.geom.Point([parseFloat(res[1]),parseFloat(res[0])])));
	    			 if (typeof(value.precio) === "undefined") {}else{
	    				 if (typeof(value.area) === "undefined") {}else{
	    					 console.log(value.area+" - "+value.precio+" - "+value.precioEnDolares);
	    					 if(value.area>0){
		    					 if(value.precioEnDolares)
		    	    				 precioM2v+=(value.precio*dlls)/value.area;
		    	    			 else
		    	    				 precioM2v+=(value.precio)/value.area;
		    					 totalM2v+=value.area;
		    					 totalLigas++;
		    	    			 }
	    					 }
	        			 }
	    		 }
	    		 else{
	    			 vectorSource3.addFeature(new ol.Feature(new ol.geom.Point([parseFloat(res[1]),parseFloat(res[0])])));
	    			 if (typeof(value.precio) === "undefined") {}else{
	    				 if (typeof(value.area) === "undefined") {}else{
	    					 console.log(value.area+" - "+value.precio+" - "+value.precioEnDolares);
	    					 if(value.area>0){
		    					 if(value.precioEnDolares)
		    						 precioM2r+=(value.precio*dlls)/value.area;
		    	    			 else
		    	    				 precioM2r+=(value.precio)/value.area;
		    					 totalM2r+=value.area;
		    					 totalLigasR++;
	    					 }
	    	    			 }
	        			 }
	    		 }
	    	 });
	    	 if(startSolr<data.response.numFound)
	    		 solr();
	    	 else{
	    		 if(totalLigas==0){
	    			 $("#vent").val(Number((0).toFixed(2)+"").toLocaleString('en'));
	    			 $("#mvent").val(Number((0).toFixed(2)+"").toLocaleString('en')); 
	    		 }else{
	    			 $("#vent").val(Number((precioM2v/totalLigas).toFixed(2)+"").toLocaleString('en'));
	    		 	 $("#mvent").val(Number((totalM2v/totalLigas).toFixed(2)+"").toLocaleString('en'));
	    		 }
	    		 if(totalLigasR==0){
	    			 $("#rent").val(Number((0).toFixed(2)+"").toLocaleString('en'));
	    			 $("#mrent").val(Number((0).toFixed(2)+"").toLocaleString('en')); 
	    		 }else{
	    			 $("#rent").val(Number((precioM2r/totalLigasR).toFixed(2)+"").toLocaleString('en'));
	    			 $("#mrent").val(Number((totalM2r/totalLigasR).toFixed(2)+"").toLocaleString('en'));
	    		 }
	    		 
	    		
	    		 startSolr=0;
	    		 precioM2v=0;
	    		 precioM2r=0;
	    		 totalLigas=0;
	    		 totalM2v=0;
	    		 totalM2r=0;
	    		 totalLigasR=0;
	    	 }
	    }

	    
	    callback2 = function(data){
	   	 $.each( data.grouped.liga.groups, function( key, value ) {
	   		eModal.iframe(value.doclist.docs[0].liga, 'Información');
	   	 });
	    }
	    
	    $("#localizacion").onEnter( function() {
	    	buscarLocalizacion();               
	    });
	    
//	    solr = function(){
//	    	$.ajax({
////	    		 url: "http://localhost:8983/solr/metrosCubicos/select?d="+parseInt($('#mts').val())/1000+"&fq={!geofilt}&indent=on&pt="+latSolr+","+lngSolr+"&q=*:*&sfield=location&start="+startSolr+"&wt=json&json.wrf=callback",
//	    		 url: "http://158.69.229.35:8983/solr/metrosCubicos/select?d="+parseInt($('#mts').val())/1000+"&fq={!geofilt}&indent=on&pt="+latSolr+","+lngSolr+"&q=*:*&sfield=location&start="+startSolr+"&wt=json&json.wrf=callback",
//	    		  dataType: "jsonp"
//	    		});
//	    }
	    
//	    solrBuscar = function(x,y){
//	    	$.ajax({
////	    		  url: "http://localhost:8983/solr/metrosCubicos/select?d=0&fq={!geofilt}&indent=on&pt="+y+","+x+"&q=*:*&group=true&group.field=liga&sfield=location&wt=json&json.wrf=callback2",
//	    		  url: "http://158.69.229.35:8983/solr/metrosCubicos/select?d=0&fq={!geofilt}&indent=on&pt="+y+","+x+"&q=*:*&group=true&group.field=liga&sfield=location&wt=json&json.wrf=callback2",
//	    		  dataType: "jsonp"
//	    		});
//	    }
	    
	    buscarLocalizacion = function() {
	    	$.ajax({
	    		url: 'https://dev.virtualearth.net/REST/v1/Locations?query='+$('#localizacion').val()+'&output=json&jsonp=searchServiceCallback&key=Ag5Sl3HMkgRbxoPJ-wZaCCHZFpQj-uOeIUeQgBQvuqnHx7nT1APozOhqCD6OYT00',
	    		dataType: "jsonp"
	    	});
	    }
	    
	    map.on('dblclick', function(evt) {
			var viewResolution = /** @type {number} */ (view.getResolution());
			mover(evt.coordinate[0],evt.coordinate[1]);
		});
	    
	    var layerSwitcher = new ol.control.LayerSwitcher({ tipLabel: 'Legend' });

	    var highlightedFeatures = [],
	    selectStyle = new ol.style.Style({
	      stroke: new ol.style.Stroke({
	        color: '#F7BE81',
	        width: 2
	      }),
	      fill: new ol.style.Fill({
	          color: 'rgba(245,200,152,0.7)'
	        }),
	      image: new ol.style.Circle({
	        radius: 5.5,
	        stroke: new ol.style.Stroke({
	          color: '#ff8200',
	          width: 2
	        }),
	        fill: new ol.style.Fill({
	            color: '#ff8200'
	          })
	      })
	    });
	    
	    
	    map.on('click', function(e) {
	 
	    	  var i;
	    	  for (i = 0; i < highlightedFeatures.length; i++) {
	    	    highlightedFeatures[i].setStyle(null);
	    	  }
	    	  highlightedFeatures = [];
	    	  map.forEachFeatureAtPixel(e.pixel, function(feature) {
	    	    feature.setStyle(selectStyle);
	    	    highlightedFeatures.push(feature);
	    	    if(feature.getProperties().geometry.s[0]==feature.getProperties().geometry.s[2] & feature.getProperties().geometry.s[1]==feature.getProperties().geometry.s[3] ){
	    	    	solrBuscar(feature.getProperties().geometry.s[0],feature.getProperties().geometry.s[1]);
	    	    }
	    	  });
	      	
	    	});
		
	    map.addControl(layerSwitcher);
	    layerSwitcher.showPanel();

	    salir = function(){
	    	window.location.href = "/ml/j_spring_security_logout";
	    }
	    
	    
var und;    	       		    
var arrayids = new Array();
var arrayimg = new Array(15);
$("#info").on("click","div.getinf", function(){
	var indima=0;
	var flag=0;
	var fillIn=false;
		var idelement= ($(this).attr('id'));
		for (i = 0; i < arrayids.length; i++)
			if(arrayids[i]==idelement){
				fillIn=true;
			}
		if(fillIn==false){
			arrayids.push(idelement);
			this.style.setProperty( 'background', 'linear-gradient( 140deg, #ffffff, #ffa366)', 'important' );
			//add feacture to vector
				var x;
				var y;
				if(idelement!=undefined){
//					if(source.feature!= undefined)
//					 source.removeFeatures(feature);
					var type=6;
//					$.getJSON( "/rest/api/1.0/getCoordinate/", { 'x': idelement, 'y': type }, function( response )
					$.getJSON( "/gp/rest/api/1.0/getRoute/", { 'p': idelement, 'x': lngSolr, 'y': latSolr}, function( response ){
						var gralcoo =  JSON.parse(response[0]);
						x= gralcoo.lat;
						y= gralcoo.lon;
				        var rutas="";
				    	$.each(gralcoo.ids, function(key,value) {
				    		rutas+=value+"\\,";
				    	});
				    	rutas= rutas.substring(0,rutas.length-(2));
				    	var indexg = arrayids.indexOf(idelement);				    	
				    	while(arrayimg[indima]!=undefined){				    		
				    		indima++;
				    	}
				    	arrayimg[indima]=idelement;
						$('#'+idelement+'icon').append("<img src=\"resources/img/point"+indima+".svg\" height=\"35\" width=\"48\"/>");
						addFeature(y, x, idelement,rutas,indima);
					});
//					window.setInterval(addRandomFeature, 5000);     
				} 
			
		}
		if(fillIn==true){
			var indexg = arrayids.indexOf(idelement);
			var indaux = arrayimg.indexOf(idelement);
			arrayimg[indaux]=und;
			if (indexg > -1) 
			    arrayids.splice(indexg, 1);
			this.style.setProperty( 'background', '#FFFFFF', 'important' );
			$('#'+idelement+'icon').empty();
			removeFeature(idelement);
		}
	});
	});

	
///----------------------------
//function Info(evt, contInfo) {
//    var i, tabcontent, tablinks;
//    tabcontent = document.getElementsByClassName("tabcontent");
//    for (i = 0; i < tabcontent.length; i++) {
//        tabcontent[i].style.display = "none";
//    }
//    tablinks = document.getElementsByClassName("tablinks");
//    for (i = 0; i < tablinks.length; i++) {
//        tablinks[i].className = tablinks[i].className.replace(" active", "");
//    }
//    document.getElementById(contInfo).style.display = "block";
//    var lay=document.getElementById(contInfo);
//    console.log(contInfo)
//    evt.currentTarget.className += " active";
//}

      
function drawChart(info, div) {
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn('string', 'Year');
    dataTable.addColumn('number', 'Sales');
    // A column for custom tooltip content
    dataTable.addColumn({type:'string', role:'style'})
    dataTable.addColumn({
    	type: 'string', role: 'tooltip'});
    dataTable.addRows(info);

    var options = {
    width: 350,
    height: 150,
    chartArea: {'width': '85%', 'height': '88%'},
    bar: {groupWidth: "95%"},
    legend: { position: "none" },
    };
    var chart = new google.visualization.ColumnChart(document.getElementById(div));
    chart.draw(dataTable, options);
  }