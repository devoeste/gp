angular.module("appMl.ACtrl").controller("ACtrl", ['$scope','$http','GralService','olData','$ngBootbox', function ($scope,$http,GralService,olData,$ngBootbox) {
    $scope.lat = 19.43798;
    $scope.lng = -99.18655;
    $scope.latSolr = 0;
    $scope.lngSolr = 0;
    $scope.startSolr = 0;
    $scope.slat= 32.7;
    $scope.slng= -86.7;
    $scope.nlat= 14.5;
    $scope.nlng= -118.4;
    $scope.control =false;
    $scope.metros=[];
    $scope.act=false;
    $scope.carga=true;
    $scope.carga2=true;
    $scope.olDat;
    $scope.markers =[];
    
    var custom_style = {
            image: {
                icon: {
                	 anchor: [0.5, 1],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'fraction',
                    opacity: 0.90,
                    src: 'resources/img/pin0.png'
                }
            }
        };
    var custom_style1 = {
            image: {
                icon: {
                	 anchor: [0.5, 1],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'fraction',
                    opacity: 0.90,
                    src: 'resources/img/pin1.png'
                }
            }
        };
    var custom_style2 = {
            image: {
                icon: {
                	 anchor: [0.5, 1],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'fraction',
                    opacity: 0.90,
                    src: 'resources/img/pin2.png'
                }
            }
        };
    
   
    searchServiceCallback = function(data){
        angular.forEach(data.resourceSets, function(value, key) {
            angular.forEach(value.resources, function(value2, key2) {
                $scope.mover(value2.point.coordinates[0],value2.point.coordinates[1]);
            });
        });
    }
    
$scope.marcadores = {
	  "type": "FeatureCollection",
	  "features": []
	};

$scope.source = new ol.source.Vector({wrapX: false});

$scope.vector = new ol.layer.Vector("Features", {
    renderers: ['Canvas','SVG']
});


    callback = function(data){
    	if($scope.startSolr==0)
    		 $scope.marcadores.features=[];
    	 $scope.startSolr+=10;
//    	 angular.forEach(data.response.docs, function(value, key) {
//    		 var res = value.location[0].split(",");
//    		 if(value.operacion == 'Venta')
//	    		 $scope.markers.push({
//	    			 	name: value.id,
//	    				lat: parseFloat(res[0]),
//	    				lon:  parseFloat(res[1]),
//	    				active: true,
//	    				draggable: false,
//	    				style: custom_style1
//	    		 	});
//    		 else
//    			 $scope.markers.push({
//	    			 	name: value.id,
//	    				lat: parseFloat(res[0]),
//	    				lon:  parseFloat(res[1]),
//	    				active: true,
//	    				draggable: false,
//	    				style: custom_style2
//	    		 	});
//    		 $scope.marcadores.features.push({
//    		      "type": "Feature",
//    		      "properties": {},
//    		      "geometry": {
//    		        "type": "Point",
//    		        "coordinates": [
//    		        	parseFloat(res[1]),
//    		          parseFloat(res[0])
//    		        ]
//    		      }
//    		    }
//    				 );
//    	 });
    	 if($scope.startSolr<data.response.numFound)
    		 $scope.solr();
    	 else{
    		 var features = [];
    		 for(var i=0; i< 1; i++) {
    		     var lon = Math.random() * 2 + -4;
    		     var lat = Math.random() * 2 + 40;

    		     var lonlat = new ol.LonLat(lon, lat);
    		     lonlat.transform(new ol.Projection("EPSG:4326"), new ol.Projection("EPSG:900913"));

    		     var f = new ol.Feature.Vector( new ol.Geometry.Point(lonlat.lon, lonlat.lat));
    		     features.push(f);
    		 }
    		 $scope.vector.addFeatures(features);
//  		     var geojsonFormat = new ol.format.GeoJSON();
//		     var features = geojsonFormat.readFeatures($scope.marcadores,{featureProjection: 'EPSG:4326'});
//		     $scope.source.addFeatures(features);
//		     $scope.layers.push( {
//		            name: "metrosCubicos",
//		            active: true,
//		            opacity: 1,
//		            source: {
//		                "type": "GeoJSON",
//		                "url": "http://tombatossals.github.io/angular-openlayers-directive/examples/json/ITA.geo.json"
//		              },
//		            style: new ol.style.Style({
//			    	    fill: new ol.style.Fill({
//			    	      color: 'rgba(255, 255, 255, 0.2)'
//			    	    }),
//			    	    stroke: new ol.style.Stroke({
//			    	      color: '#ffcc33',
//			    	      width: 2
//			    	    }),
//			    	    image: new ol.style.Circle({
//			    	      radius: 7,
//			    	      fill: new ol.style.Fill({
//			    	        color: '#ffcc33'
//			    	      })
//			    	    })
//		    	  })
//		     	});
//		     console.log($scope.layers);
    		 $scope.startSolr=0;
    	 }
    }

    $scope.solr = function(){
    	$http.jsonp("http://158.69.229.35:8983/solr/metrosCubicos/select?d="+($scope.distancia.value/1000)+"&fq={!geofilt}&indent=on&pt="+$scope.latSolr+","+$scope.lngSolr+"&q=*:*&sfield=location&start="+$scope.startSolr+"&wt=json&json.wrf=callback")
    }
    
    $scope.buscarLocalizacion = function() {
    	$http.jsonp('https://dev.virtualearth.net/REST/v1/Locations?query='+$scope.localizacion+'&output=json&jsonp=searchServiceCallback&key=Ag5Sl3HMkgRbxoPJ-wZaCCHZFpQj-uOeIUeQgBQvuqnHx7nT1APozOhqCD6OYT00');
    }
    var geometryFunction, maxPoints;



    
    $scope.geocontrol= function(d){
    	resultado="";
    	angular.forEach(d, function(value, key) {
    		resultado+=value+"\\,";
    	});
    	resultado= resultado.substring(0,resultado.length-2);
    	$scope.layers.push( {
            name: "Tipologia",
            active: true,
            opacity: 1,
            source: {
            	type: 'TileWMS',
                url: 'http://158.69.229.35/geoserver/Oeste/wms',
                params: { 
                	layers: 'Oeste:layer_ml',
		            format: 'image/png',
		            opacity: 0.7,
		            transparent: true,
		            STYLES:'ml',
		            viewparams:'tipo:4;list:'+resultado
                }
            }
    });
//			olData.getMap().then(function(map) {
//				$scope.center.lon = map.getView().getCenter()[0];
//                $scope.center.lat = map.getView().getCenter()[1];
//                $scope.zoom = map.getView().getZoom();
//			});
    }
    
 

    
 $scope.distancia = {
    value: 100,
    options: {
      floor: 100,
      ceil: 750,
      onChange: function(sliderId) {
    //   $scope.paths.c2.radius=$scope.distancia.value;
   	 
    }
}
};
 



var w = true;
$scope.mover = function(lat,lng){
	var w = [];
	 angular.forEach( $scope.markers,function(value2, key2) {
		 if(value2.draggable==false)
			 w.push(key2);

	 });
	 angular.forEach( w,function(value2, key2) {
		 delete $scope.markers[value2];
	 });	
    $scope.lat = lat;
    $scope.lng = lng;
    if($scope.markers.length==0){
		$scope.markers.push({
			name: "Point",
			lat: lat,
			lon:  lng,
			active: true,
			draggable: true
			,style: custom_style
			});
//		if(w)
//			olData.getMap().then(function (map) {
//			draw = new ol.interaction.Draw({
//	            source:  $scope.source,
//	            type: /** @type {ol.geom.GeometryType} */ 'Circle',
//	            geometryFunction: geometryFunction,
//	            maxPoints: maxPoints
//	          });
//	          map.addInteraction(draw);
//	          w=false;
//			});

	}else{
		$scope.markers[0].lat=lat;
		$scope.markers[0].lon=lng;
	}
    
    
    olData.getMap().then(function(map) {
    	var eee =map.getLayers();
        angular.forEach(eee, function(value, key) {
//        	if(angular.isUndefined(value.B.name)){
        		if(angular.isUndefined(value.n.name)){	
        	}else{
        		if(value.n.name=='Tipologia')
//        			if(value.B.name=='Tipologia')
        			eee.remove(value);
        	}
        });
    });

    GralService.dataSpacial(lng,lat,$scope.distancia.value).then(
			function(response){

					$('.gridc').show();
					$scope.act=true;
					$scope.gral =  angular.fromJson(response.data[0]);
					$scope.geocontrol($scope.gral.id_tipologia[0].ci_capa_geom_id);
					if($scope.gral.comercios_giro!= null){
					var x = AmCharts.makeChart("chartdiv", {
					 "type": "serial",
					  "theme": "dark",
					  "rotate":true,
					  "dataProvider": $scope.gral.comercios_giro,
					  "valueAxes": [ {
					    "gridColor": "#FFFFFF",
					    "gridAlpha": 0.2,
					    "dashLength": 0
					  } ],
					  "gridAboveGraphs": true,
					  "startDuration": 5,
					  "graphs": [ {
					    "fillAlphas": 0.8,
					    "lineAlpha": 0.2,
					    "type": "column",
					    "valueField": "n_comercios"
					  } ],
					  "chartCursor": {
					    "categoryBalloonEnabled": false,
					    "cursorAlpha": 0,
					    "zoomable": true
					  },
					  "categoryField": "desc_actividad",
					  "categoryAxis": {
					    "gridPosition": "start",
					    "gridAlpha": 0,
					    "tickPosition": "start",
					    "tickLength": 20
					  },
					  "export": {
					    "enabled": false
					  }
					});
					}
					if($scope.gral.tipologias_region!= null)
					var y = AmCharts.makeChart( "chartdiv2", {
						  "type": "pie",
						  "theme": "light",
						  "dataProvider": $scope.gral.tipologias_region,
						  "titleField": "nombre_tip",
						  "valueField": "n_tipologias",
						  "labelRadius": 5,

						  "radius": "42%",
						  "innerRadius": "60%",
						  "labelText": "[[nombre_tip]]",
						  "export": {
						    "enabled": false
						  },
						  "clickSlice" : function(dataItem,event) {
							     console.log(dataItem.value);
							     console.log(dataItem.dataContext);
							     var a =dataItem.dataContext.descripcion;
							     var options = {
							    	        message: a,
							    	        title: dataItem.dataContext.nombre_tip,
							    	        className: 'test-class',
							    	        buttons: {
							    	             success: {
							    	                 label: "Ok",
							    	                 className: "btn-success",
							    	                 callback: function() { }
							    	             }
							    	        }
							    	    };

							    	$ngBootbox.customDialog(options);
							     event.stopPropagation();
							   }
						});
					if($scope.gral.edad_poblacion!= null){
					var z = AmCharts.makeChart( "chartdiv3", {
						  "type": "pie",
						  "theme": "light",
						  "dataProvider": $scope.gral.edad_poblacion,
						  "titleField": "grupo",
						  "valueField": "n_gente",
						  "labelRadius": 5,

						  "radius": "42%",
						  "innerRadius": "45%",
						  "labelText": "[[grupo]]",
						  "export": {
						    "enabled": false
						  }
						});
						var ew=0; 
						angular.forEach($scope.gral.edad_poblacion, function(value, key) {
							ew+=value.n_gente;
						});
						$('#edad').empty().append(ew);
						}
					if($scope.gral.educacion_poblacion!= null){
					var zz = AmCharts.makeChart( "chartdiv4", {
						  "type": "pie",
						  "theme": "light",
						  "dataProvider": $scope.gral.educacion_poblacion,
						  "titleField": "grupo",
						  "valueField": "n_gente",
						  "labelRadius": 5,

						  "radius": "42%",
						  "innerRadius": "45%",
						  "labelText": "[[grupo]]",
						  "export": {
						    "enabled": false
						  }
						});
						var ew=0; 
						angular.forEach($scope.gral.educacion_poblacion, function(value, key) {
							ew+=value.n_gente;
						});
						$('#edu').empty().append(ew);
					}
					var gasto0=[];
					angular.forEach($scope.gral.gasto, function(value, key) {
						if(value.mean_gasto!=0)
							gasto0.push(value);
					});
					if(gasto0.length >0)
					var zz2 = AmCharts.makeChart( "chartdiv5", {
						 "type": "serial",
						  "theme": "dark",
						  "rotate":true,
						  "dataProvider": gasto0,
						  "valueAxes": [ {
						    "gridColor": "#FFFFFF",
						    "gridAlpha": 0.2,
						    "dashLength": 0
						  } ],
						  "gridAboveGraphs": true,
						  "startDuration": 5,
						  "graphs": [ {
						    "fillAlphas": 0.8,
						    "lineAlpha": 0.2,
						    "type": "column",
						    "valueField": "mean_gasto"
						  } ],
						  "chartCursor": {
						    "categoryBalloonEnabled": false,
						    "cursorAlpha": 0,
						    "zoomable": true
						  },
						  "categoryField": "nombre",
						  "categoryAxis": {
						    "gridPosition": "start",
						    "gridAlpha": 0,
						    "tickPosition": "start",
						    "tickLength": 20
						  },
						  "export": {
						    "enabled": false
						  }
						});
					var ingreso0=[];
					angular.forEach($scope.gral.ingreso, function(value, key) {
						if(value.mean_ingreso!=0)
							ingreso0.push(value);
					});
					if(ingreso0.length >0)
					var zz3 = AmCharts.makeChart( "chartdiv6", {
						 "type": "serial",
						  "theme": "dark",
						  "rotate":true,
						  "dataProvider": ingreso0,
						  "valueAxes": [ {
						    "gridColor": "#FFFFFF",
						    "gridAlpha": 0.2,
						    "dashLength": 0
						  } ],
						  "gridAboveGraphs": true,
						  "startDuration": 5,
						  "graphs": [ {
						    "fillAlphas": 0.8,
						    "lineAlpha": 0.2,
						    "type": "column",
						    "valueField": "mean_ingreso"
						  } ],
						  "chartCursor": {
						    "categoryBalloonEnabled": false,
						    "cursorAlpha": 0,
						    "zoomable": true
						  },
						  "categoryField": "nombre",
						  "categoryAxis": {
						    "gridPosition": "start",
						    "gridAlpha": 0,
						    "tickPosition": "start",
						    "tickLength": 20
						  },
						  "export": {
						    "enabled": false
						  } 
						});
						if(ingreso0.length==0 | gasto0.length==0)
							$('#gastos').hide();
						
						angular.forEach($('a'), function(value, key) {
							if(value.href=="http://www.amcharts.com/javascript-charts/")
								value.remove();
						});
						 $scope.latSolr = lat;
						 $scope.lngSolr = lng;
						 $scope.solr();
				    
			}, 
			function(errResponse){
				return errResponse;
			}
	);   
}


angular.extend($scope, {
    center: {
        lat: $scope.lat,
        lon: $scope.lng,
        zoom: 15,
        bounds: [ 32.7,-86.7, 14.5,-118.4],
        projection: 'EPSG:4326',
    },
    markers: $scope.markers,
    layers: [
    	{
		name: 'Bing',
		active: true,
    	opacity: 1,
         source: {
             type: 			'BingMaps',
             key: 			'Ag5Sl3HMkgRbxoPJ-wZaCCHZFpQj-uOeIUeQgBQvuqnHx7nT1APozOhqCD6OYT00',
             imagerySet: 	'Road'
         },
    	},
    	$scope.vector
	],
    defaults: {
    	view:{
	        maxZoom: 20,
	        minZoom:5,
    	},
        events: {
            map: [ 'singleclick', 'pointermove' ]
            },
    	interactions: {
    		mouseWheelZoom: true
    		},
    	controls: [
                   { name: 'zoom', active: true },
                   { name: 'fullscreen', active: true }
               ]
    },
    mouseposition: {},
    mouseclickposition: {},
    projection: 'EPSG:4326'
});


$scope.init = function(){
	$scope.olDat = olData;
	$('.gridc').hide();
	$scope.carga2=false;
}

	$scope.$on('openlayers.map.singleclick', function(event, data) {
	    $scope.$apply(function() {
	    		var x;var y;
	    		var resolution = /** @type {number} */ data.event.map.getView().getResolution();
	            if ($scope.projection === data.projection) {
	          	  y=data.coord.lat;
	              x=data.coord.lon;
	          } else {
	              var p = ol.proj.transform([ data.coord[0], data.coord[1] ], data.projection, $scope.projection);
	              y=p[1];
	              x=p[0];
	          }
	        	$scope.mover(y,x);
	      });
	});


}]);
