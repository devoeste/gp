<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page session="true"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
  	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="https://openlayers.org/en/v4.0.1/css/ol.css" type="text/css">
	<link rel="icon" type="image/ico" href="resources/images/favicon.ico"/>
    <title>Mejor Lugar - Oeste.mx </title>
    <link href="resources/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
 	<link href="resources/css/app/style.css" rel="stylesheet">
 	<link href="resources/css/app/loading.css" rel="stylesheet">
 	<link href="resources/plugins/rangeslider/rangeslider.css" rel="stylesheet">
	<link href="resources/plugins/ol3/ol.css" rel="stylesheet">
	<link href="resources/plugins/ol3/ol3-layerswitcher.css" rel="stylesheet">

  </head>
  <body style="background-color:#f9f8f8;">
  	<div class="container-fluid" style="padding-left:0 !important;" style="display:none;">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default navbar-fixed-top navbar-oeste" role="navigation">

				<div class="row">
					<div class="navbar-header col-sm-3">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							 <span class="sr-only"></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
						</button>
						<a class="navbar-brand navBarlogo" style="display: -webkit-box;" href="#">
  						<img src="resources/img/logo_oeste.png"  class="img-responsive mllogo" alt="Image" /> 
							<span  class="img-responsive mllogo" style="margin-left: 10px;margin-top: 2px;">Mejor Ubicación</span>
						</a>
					</div>
					<div class="navbar-header col-sm-6 " style="display: inline-flex; margin-top: 7px;" class="btn-toolbar tooloeste row">
						 <div class="row" style="display: inline-flex;">
							 <div class="col-md-4" style="margin-top: 6px;">
									<input name="range" type="range" min="2500" max="5000" step="10">
							 </div>
							 <div class="input-group col-md-2">
				  				<input type="text" class="form-control" id="mts" readonly=true>
				  				<span class="input-group-addon">m</span>
							</div>
							<div class="col-md-5">
							 	    <div class="input-group">
			      						<input type="text" class="form-control" id="localizacion" placeholder="buscar dirección...">
			     						<span class="input-group-btn">
			        						<button class="btn btn-default" onClick="buscarLocalizacion()" type="button">Ir!</button>
			      						</span>
			    					</div>
							 </div>
						</div>
					</div>
					<div class=" navbar-collapse col-sm-3" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right botonesoeste" style="margin-left: 30px !important;float: left;margin-top: 7px !important;">
							<li class="">
								<button class="btn btn-default" onClick="salir()" style="margin-left: 30px !important;float: left;">Salir</button>
							</li>
						</ul>
					</div>
					</div>
				</nav>
				<img src="resources/img/logo_oeste.png" class="img-responsive powered" alt="Image" />
			</div>
		</div>

		<div id="contenedor" class="contenedor" style="margin-top: 55px;" >
			<div class=row>
				<div id="map" class="andular-openlayers-map ng-isolate-scope col-sm-8">
				</div>
				<div  id= "info" class="gridc row col-sm-12">
					<div class="tab">
  						<a href="javascript:void(0)" class="tablinks"  data="Municipios" data-index="2">
  							<span><img src="resources/img/municipio.svg"  class="img-responsive mllogo" alt="Municipio" style="width:30px;height:30px;" /></span>
  						</a>
    					<a href="javascript:void(0)" class="tablinks"  data="Hoteles" data-index="8">
    						<span><img src="resources/img/hote.svg"  class="img-responsive mllogo" alt="HoteL" style="width:30px;height:30px;" /></span>
    					</a>
  						<a href="javascript:void(0)" class="tablinks"  data="PrecioRenta" data-index="3">
  							<span><img src="resources/img/renta.svg"  class="img-responsive mllogo" alt="Renta" style="width:30px;height:30px;" /></span>
  						</a>
  						<a href="javascript:void(0)" class="tablinks"  data="PrecioVenta" data-index="4">
  							<span><img src="resources/img/venta.svg"  class="img-responsive mllogo" alt="Venta" style="width:30px;height:30px;" /></span>
  						</a>
    					<a href="javascript:void(0)" class="tablinks"  data="GeneradoresDemanda" data-index="9">
    						<span><img src="resources/img/industria.svg"  class="img-responsive mllogo" alt="Industria" style="width:30px;height:30px;" /></span>
    					</a>
      					<a href="javascript:void(0)" class="tablinks"  data="RedFerrea" data-index="6">
      						<span><img src="resources/img/ferrea.svg"  class="img-responsive mllogo" alt="Ferrea" style="width:30px;height:30px;" /></span>
      					</a>
        				<a href="javascript:void(0)" class="tablinks"  data="Carreteras" data-index="5">
        					<span><img src="resources/img/carretera.svg"  class="img-responsive mllogo" alt="Carretera" style="width:30px;height:30px;" /></span>
        				</a>
          				<a href="javascript:void(0)" class="tablinks"  data="Aeropuertos" data-index="7">
          					<span><img src="resources/img/aeropuerto.svg"  class="img-responsive mllogo" alt="Aeropuerto" style="width:30px;height:30px;" /></span>
          				</a>
          				<a href="javascript:void(0)" class="tablinks"  data="Parques" data-index="8">
          					<span><img src="resources/img/aeropuerto.svg"  class="img-responsive mllogo" alt="Parques" style="width:30px;height:30px;" /></span>
          				</a>
          				<a href="javascript:void(0)" class="tablinks"  data="Topologias" data-index="10">
          					<span><img src="resources/img/topologias.svg"  class="img-responsive mllogo" alt="Topologias" style="width:30px;height:30px;" /></span>
          				</a>
					</div>
					<div id="Municipios" class="tabcontent">
						<h4 id="th4">Municipios</h4>
  							<div id="dataMunicipios"> 									
							</div>														
					</div>
					<div id="Hoteles" class="tabcontent">
						<h4 id="th4">Hoteles</h4>
  						<div id="scroll" >
  							<div id="dataHoteles"> 									
							</div>														
  						</div>
					</div>
					<div id="PrecioRenta" class="tabcontent">
						<h4 id="th4">Precio de renta en la zona</h4>
  						<div id="dataRenta" class='getinfs'> 									
																				
  						</div>
					</div>
					<div id="PrecioVenta" class="tabcontent">
						<h4 id="th4">Precio de venta en la zona</h4>
  						<div id="dataVenta" class='getinfs'> 									
																				
  						</div>
					</div>
					<div id="GeneradoresDemanda" class="tabcontent">
  						<h4 id="th4">Generadores de demanda</h4>
  							<div  id="scroll">
  								<div id="dataGen"> 									
								</div>									  							
  							</div>	
					</div>
					<div id="RedFerrea" class="tabcontent">
  						<h4 id="th4">Distancia a la red Ferrea</h4>						
  								<div id="dataFe"> 									
								</div>									  								
					</div>
					<div id="Carreteras" class="tabcontent">
  						<h4 id="th4">Distancia a carreteras</h4>
  								<div id="dataCa"> 									
								</div>									  							
					</div>
					<div id="Aeropuertos" class="tabcontent">
  						<h4 id="th4">Distancia a aeropuerto</h4>
  								<div id="dataAe"> 									
								</div>									  							
					</div>
					<div id="Parques" class="tabcontent">
						<h4 id="th4">Parques Industriales</h4>
  						<div id="scroll" >
  							<div id="dataParques"> 									
							</div>														
  						</div>
					</div>
					<div id="Topologias" class="tabcontent">
						<h4 id="th4">Tipologia de la zona</h4>
  						<div id="dataTo" class='getinfs'> 																							
  						</div>
					</div>
    			</div>
    		</div>
		</div>
	<script src="https://openlayers.org/en/v4.0.1/build/ol.js"></script>
	<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script> 
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src='resources/plugins/jquery/dist/jquery.min.js'></script>
	<script src='resources/plugins/ol3/ol.js'></script>
	<script src="resources/plugins/ol3/ol3-layerswitcher.js"></script>
	<script src="//rawgit.com/saribe/eModal/master/dist/eModal.min.js"></script>
    <script src='resources/plugins/bootstrap/dist/js/bootstrap.min.js'></script>
	<script src='resources/plugins/ngBootbox/bootbox.min.js'></script>
	<script src="resources/plugins/rangeslider/rangeslider.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/amcharts.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/pie.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/funnel.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/serial.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/themes/light.js"></script>
 	<script src="resources/app/Controller/IndexCtrl2.js"></script> 
	
  </body>
</html>