<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page session="true"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/ico" href="resources/images/favicon.ico"/>
    <title>Mejor Lugar - Oeste.mx </title>
    <link href="resources/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
 	<link href="resources/css/app/style.css" rel="stylesheet">
 	<link href="resources/css/app/loading.css" rel="stylesheet">
 	<link href="resources/plugins/rangeslider/rangeslider.css" rel="stylesheet">
	<link href="resources/plugins/ol3/ol.css" rel="stylesheet">
	<link href="resources/plugins/ol3/ol3-layerswitcher.css" rel="stylesheet">
  </head>
  <body style="background-color:#f9f8f8;">
  	<div class="container-fluid" style="padding-left:0 !important;" style="display:none;">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default navbar-fixed-top navbar-oeste" role="navigation">
					<div class="navbar-header">
						<a class="navbar-brand navBarlogo" style="display: -webkit-box;" href="#">
  						<img src="resources/img/logo_oeste.png"  class="img-responsive mllogo" alt="Image" /> 
							<span  class="img-responsive mllogo" style="margin-left: 10px;margin-top: 2px;">Mejor Ubicación</span>
						</a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right botonesoeste" style="margin-right: 100px !important;margin-top: 7px !important;">
							<li>
							
							</li>
							<li class="">
								<button class="btn btn-default" onClick="salir()">Salir</button>
							</li>
						</ul>
					</div>
				</nav>
				<img src="resources/img/logo_oeste.png" class="img-responsive powered" alt="Image" />
			</div>
		</div>

		<div id="contenedor" class="contenedor" >
				<div class="row">
				<div class="col-md-6">
			    	<div style="display: inline-flex;" class="btn-toolbar tooloeste row">
						 <div class="row" style="display: inline-flex;">
							 <div class="col-md-4" style="margin-top: 6px;">
									<input name="range" type="range" min="10" max="750" step="10">
							 </div>
							 <div class="input-group col-md-2">
				  				<input type="text" class="form-control" id="mts" readonly=true>
				  				<span class="input-group-addon">m</span>
							</div>
							<div class="col-md-6">
							 	    <div class="input-group">
			      						<input type="text" class="form-control" id="localizacion" placeholder="buscar dirección...">
			     						<span class="input-group-btn">
			        						<button class="btn btn-default" onClick="buscarLocalizacion()" type="button">Ir!</button>
			      						</span>
			    					</div>
							 </div>
						</div>
					</div>
					</div>
				</div>
				<div class=row>
				<div id="map" class="andular-openlayers-map ng-isolate-scope col-sm-8">
				</div>
				<div  id= "info" class="gridc row col-sm-4">
		    			<div>
		    	 			<div class="row">
  								<div class="col-sm-4">
  									<h4 id="th4">Municipio</h4>
  								</div> 						
							</div>
						<div class="row">
  						<div class="col-sm-12">
  							<h4 id="th4">Hoteles</h4>
  							<div id="scroll" >
  								<div id="dataHoteles"> 									
								</div>  							
  							</div>
  						</div> 						
					</div>
					<div class="row">
  						<div class="col-sm-12">
  							<h4 id="th4">Precio Renta</h4>
  						</div> 						
					</div>
					<div class="row">
  						<div class="col-sm-12">
  							<h4 id="th4">Precio Venta</h4>
  						</div> 						
					</div>
					<div class="row">
  						<div class="col-sm-12">
  							<h4 id="th4">Generadores de demanda</h4>
  								<div  id="scroll">
  									<div id="dataGen"> 									
									</div>  							
  								</div>
  						</div> 						
					</div>
					<div class="row">
  						<div class="col-sm-12">
  							<h4 id="th4">Distancia de la red ferrea</h4>
  							  	<div id="distfe"> 									
								</div>  
  						</div> 						
					</div>
					<div class="row">
  						<div class="col-sm-12">
  							<h4 id="th4">Distancia de la carretera</h4>
  							<div id="distca"> 									
								</div>  
  						</div> 						
					</div>
					<div class="row">
  						<div class="col-sm-12">
  							<h4 id="th4">Distancia del aeropuerto</h4>
  							<div id="distae"> 									
								</div>  
  						</div> 						
					</div>					
				</div>
    </div>
    </div>

    </div>
    <script src='resources/plugins/jquery/dist/jquery.min.js'></script>
	<script src='resources/plugins/ol3/ol.js'></script>
	<script src="resources/plugins/ol3/ol3-layerswitcher.js"></script>
	<script src="//rawgit.com/saribe/eModal/master/dist/eModal.min.js"></script>
    <script src='resources/plugins/bootstrap/dist/js/bootstrap.min.js'></script>
	<script src='resources/plugins/ngBootbox/bootbox.min.js'></script>
	<script src="resources/plugins/rangeslider/rangeslider.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/amcharts.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/pie.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/funnel.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/serial.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/themes/light.js"></script>
 	<script src="resources/app/Controller/IndexCtrl2.js"></script> 
	
  </body>
</html>