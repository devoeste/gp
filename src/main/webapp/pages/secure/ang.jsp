
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page session="true"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/ico" href="resources/images/favicon.ico"/>
    <title>Mejor Lugar - Oeste.MX </title>
    <link href="resources/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
 	<link href="resources/css/app/style.css" rel="stylesheet">
 	<link href="resources/css/app/loading.css" rel="stylesheet">
<!--  	<link href="resources/plugins/ol3/ol.css" rel="stylesheet"> -->.
<link href="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.18.2/ol.css" rel="stylesheet">
<!-- 	<link href="http://angular-ui.github.io/ui-leaflet/bower_components/leaflet/dist/leaflet.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
	<link href="resources/plugins/rzSlider/rzSlider.css" rel="stylesheet">
  </head>
  <body ng-app="appMl" ng-controller="ACtrl" ng-init="init()" style="background-color:#fff;">
  	<div class="container-fluid" style="padding-left:0 !important;" ng-show="!carga2">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default navbar-fixed-top navbar-oeste" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							 <span class="sr-only"></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
						</button>
						<a class="navbar-brand navBarlogo" style="display: -webkit-box;" href="#">
  						<img src="resources/img/logo_oeste.png"  class="img-responsive mllogo" alt="Image" /> 
							<span  class="img-responsive mllogo" style="margin-left: 10px;margin-top: 2px;">Mejor Ubicación</span>
						</a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right botonesoeste">
							<li class="">
								
							</li>
						</ul>
					</div>
				</nav>
				<img src="resources/img/logo_oeste.png" class="img-responsive powered" alt="Image" />
			</div>
		</div>

<!-- 		<div id="contenedor" class="contenedor" > -->
<!-- 				<div class="row"> -->
<!-- 			    	<div style="display: inline-flex;" class="btn-toolbar tooloeste row"> -->
<!-- 						 <div class="row" style="display: inline-flex;"> -->
<!-- 							 <div class="col-md-4"> -->
<!-- 							 	<rzslider rz-slider-model="distancia.value" rz-slider-options="distancia.options"></rzslider> -->
<!-- 							 </div> -->
<!-- 							 <div class="input-group col-md-2"> -->
<!-- 				  				<input type="text" class="form-control" ng-model="distancia.value" ng-readonly=true> -->
<!-- 				  				<span class="input-group-addon">m</span> -->
<!-- 							</div> -->
<!-- 							<div class="col-md-6"> -->
<!-- 							 	    <div class="input-group"> -->
<!-- 			      						<input type="text" class="form-control" ng-model="localizacion" placeholder="buscar dirección..."> -->
<!-- 			     						<span class="input-group-btn"> -->
<!-- 			        						<button class="btn btn-default" ng-click="buscarLocalizacion()" type="button">Ir!</button> -->
<!-- 			      						</span> -->
<!-- 			    					</div> -->
<!-- 							 </div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- <!-- 		    <openlayers ol-center="center"  ol-defaults="defaults" custom-layers="true" style="width:70vw; height:70vh;margin:0 auto 0 auto;"> --> -->
<!-- <!-- 			 	<ol-marker ol-marker-properties="marker" ng-hide="marker.active" ></ol-marker> --> -->
<!-- <!-- 				<ol-control name="{{ control.name }}" ng-repeat="control in controls|filter: {active: true}"></ol-control> --> -->
<!-- <!-- 				<ol-marker ol-marker-properties="marker" ng-repeat="marker in markers|filter:{active:true}"></ol-marker> --> -->
<!-- <!-- 		     	<ol-layer ol-layer-properties="layer" ng-repeat="layer in layers"></ol-layer> --> -->
<!-- <!-- 		    </openlayers> --> -->
<!-- <!-- 		    <leaflet bounds="bounds" maxbounds="maxbounds" defaults="defaults" paths="paths" layers="layers" center="osloCenter" markers="markers" event-broadcast="events" style="width:70vw; height:70vh;margin:0 auto 0 auto;"></leaflet> --> -->
<!-- 		    <div class="gridc"> -->
<!-- 		   		<h3 style="text-align: center;">Tipologias de la región</h3> -->
<!-- 				<div id="chartdiv2" style="height:250px;width:auto;" ></div> -->
<!-- 			</div> -->
<!-- 		    <div class="gridc" > -->
<!-- 		    	<h3 style="text-align: center;">Comercios de la región por giro</h3> -->
<!-- 				<div id="chartdiv" style="height:400px;width:auto;" ></div> -->
<!-- 			</div> -->
<!-- 			<div class="gridc row" > -->
<!-- 				<div  width: 50%;" class="col-md-6" >  -->
<!-- 					<h3 style="text-align: center;">Edad de la población</h3> -->
<!-- 					<div id="chartdiv3"  style="height:250px;width:auto;" ></div> -->
<!-- 					<span  class="icon-btn">Total:&nbsp<small id="edad">0</small></span> -->
<!-- 				</div> -->
<!-- 				<div  width: 50%;" class="col-md-6"> -->
<!-- 					<h3 style="text-align: center;">Educación</h3> -->
<!-- 					<div id="chartdiv4" style="height:250px;width:auto;" ></div> -->
<!-- 					<span  class="icon-btn" >Total:&nbsp<small id="edu">0</small></span> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 			<div class="gridc row" id="gastos" > -->
<!-- 				<div  width: 50%;" class="col-md-6" >  -->
<!-- 					<h3 style="text-align: center;">Gasto</h3> -->
<!-- 					<div id="chartdiv5" style="height:250px;width:auto;" ></div> -->
<!-- 				</div> -->
<!-- 				<div width: 50%;" class="col-md-6"> -->
<!-- 					<h3 style="text-align: center;">Ingreso</h3> -->
<!-- 					<div id="chartdiv6" style="height:250px;width:auto;" ></div> -->
<!-- 				</div> -->
<!-- 			</div> -->
			
    </div>
    </div>
    <script src='resources/plugins/jquery/dist/jquery.min.js'></script>
<!--     <script src='http://tombatossals.github.io/angular-leaflet-directive/bower_components/leaflet/dist/leaflet.js'></script> -->
	<script src='http://tombatossals.github.io/angular-openlayers-directive/bower_components/openlayers3/build/ol.js'></script>
<!-- 	<script src='https://cdnjs.cloudflare.com/ajax/libs/ol3/3.11.2/ol.min.js'></script> -->
    <script src='resources/plugins/bootstrap/dist/js/bootstrap.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-animate.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-touch.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-route.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-sanitize.min.js'></script>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/amcharts.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/serial.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/pie.js"></script>
	<script src="https://www.amcharts.com/lib/3/funnel.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/themes/light.js"></script>
	<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
	<script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
	<script src='resources/plugins/ngBootbox/bootbox.min.js'></script>
<!-- 	<script src='resources/plugins/ngBootbox/ngBootbox.min.js'></script> -->

	<script src="https://rawgit.com/rzajac/angularjs-slider/master/dist/rzslider.js"></script>
<!-- 	<script src='resources/plugins/ui-leaflet/angular-simple-logger.min.js'></script> -->
<!-- 	<script src='http://angular-ui.github.io/ui-leaflet/bower_components/leaflet-plugins/layer/tile/Bing.js'></script> -->
<!-- 	<script src='resources/plugins/ui-leaflet/ui-leaflet.js'></script> -->
<!-- 	<script src='resources/plugins/ol3/angular-openlayers-directive.min.js'></script> -->
<!-- 	<script src='http://tombatossals.github.io/angular-openlayers-directive/dist/angular-openlayers-directive.min.js'></script> -->
<!--  	<script src='resources/app/Module/appIndex.js'></script> -->
<!--  	<script src='resources/app/Service/GralService.js'></script>  -->
 	<script src='resources/app/Controller/IndexCtrl.js'></script> 
	
  </body>
</html>