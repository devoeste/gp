<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page session="true"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appLogin">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/ico" href="resources/images/favicon.ico"/>
    <title>Mejor Lugar - Oeste.MX </title>

    <link href="resources/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">

 	<link href="resources/css/app/login.css" rel="stylesheet">
 	  
  </head>
  <body class="login" ng-controller="loginCtrl" ng-init="init();">
    <div>
      <div class="login_wrapper container">
        <div class="animate form login_form">
          <section class="login_content">
            <form name='loginForm' class="form-signins"  action="<c:url value='/j_spring_security_check' />" method='POST'>
<!--               <img class="img-responsive" src="resources/images/QoGpVAQ2.fw.png" /> -->
              <h1 style="text-align: center;">Inicio de sesión</h1>
              <div>
                <input type="text"  name='username' id='username' class="form-control" placeholder="Usuario" required autofocus autocomplete="off" />
              </div>
              <div>
                <input type="password"  name="password" id="password"  class="form-control" placeholder="Contraseña" required autocomplete="off" />
              </div>
              <div class="alert col-md-12 col-lg-12 col-xs-12" role="alert" ng-show="muestra" style="color: #a94442; background-color: #f2dede; border-color: #ebccd1;">
			  	<span class="fa fa-info-circle" aria-hidden="true"></span> Credenciales erroneas
			  </div>
              <div>
                <button name="submit" type="submit" style="    position: relative;
    margin: -20px -50px;
    top: 50%;
    left: 50%;" class="btn btn-default">Iniciar sesión</button>
              </div>
              <div class="clearfix"></div>
              <div class="separator">
                <div class="clearfix"></div>
                <br />
                <div>
                  <p style="text-align: center;">©2016 Todos los derechos reservados.<br /> Oeste.Mx<br /></p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
    
    <script src='resources/plugins/jquery/dist/jquery.min.js'></script>
	<script src='resources/plugins/bootstrap/dist/js/bootstrap.min.js'></script>
	<script src='resources/plugins/angular/angular.min.js'></script>
	<script src='resources/plugins/angular/angular-route.js'></script>
	
	<script src='resources/app/Module/appLogin.js'></script>
	<script src='resources/app/Controller/loginCtrl.js'></script>
  </body>
</html>